const express = require('express');
const app = express();
const graphql = require ('graphql');
const Sequelize = require('sequelize');
const expressGraphQl = require('express-graphql');


const sequelize = new Sequelize ('office', 'root', 'Divya@1994',{
    logging: false,
    host: 'localhost',
    dialect:"mysql",
    define: {
     timestamps: false
  },
    pool: {
     max: 5,
     min: 0,
     acquire: 30000,
     idle: 10000
   }
  })
  sequelize
  .authenticate()
  .then(()  =>{
    console.log('Connection has been established successfully');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  const Model = Sequelize.Model;
  class Company extends Model {}
  Company.init({
    id:{
      type:Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    company_name: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'companies'
  });

//   Company.sync({ force: true }).then(() => {
//         return Company.create({
//       company_name: 'ABC',
//         })
//       });


class Role extends Model {}
  Role.init({
    id:{
      type:Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    role_type: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'roles'
  });

//   Role.sync({ force: true }).then(() => {
//             return Role.create({
//           role_type: 'Manager',
//             })
//           });

class Project extends Model {}
  Project.init({
    id:{
      type:Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    project_name: {
      type: Sequelize.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'projects'
  });

// Project.sync({ force: true }).then(() => {
//     return Project.create({
//     Project_name: 'Testing Project',
//     })
//     });

class Employee extends Model {}
  Employee.init({
    id:{
      type:Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    employee_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    company_id:{
        type: Sequelize.INTEGER,
        allowNull:true,
        references:{
          model:'companies',
          key:'id'
        }
      },
      role_id:{
        type: Sequelize.INTEGER,
        allowNull:true,
        references:{
          model:'roles',
          key:'id'
        }
      },
      project_id:{
        type: Sequelize.INTEGER,
        allowNull:true,
        references:{
          model:'projects',
          key:'id'
        }
      },
  }, {
    sequelize,
    modelName: 'employees'
  });

  // Employee.sync({ force: true }).then(() => {
  //           return Employee.create({
  //         employee_name: 'Santhosh',
  //           })
  //         });

//Company_Project
class Project_company extends Model {}
Project_company.init({
    id:{
      type:Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
      project_id:{
        type: Sequelize.INTEGER,
        allowNull:true,
        references:{
          model:'projects',
          key:'id'
        }
      },
      company_id:{
        type: Sequelize.INTEGER,
        allowNull:true,
        references:{
          model:'companies',
          key:'id'
        }
      },
  }, {
    sequelize,
    modelName: 'project_companies'
  });

//   Project_company.sync({ force: true }).then(() => {
//     return Project_company.create()
//     });  

//Project Status
class Status extends Model {}
  Status.init({
    id:{
      type:Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    project_status: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'status'
  });

  // Status.sync({ force: true }).then(() => {
  //  return Status.create({
  //   project_status: 'Under Discussion',
  //       })
  //            });



//Relations
// Project.hasMany(Project_company,{foreignkey:'project_id'})
Project_company.belongsTo(Project,{foreignKey:'project_id'})
// Company.hasMany(Project_company,{foreignkey:'company_id'})
Project_company.belongsTo(Company,{foreignKey:'company_id'})
Company.belongsToMany(Project,{through:Project_company,foreignKey:'company_id'})
Project.belongsToMany(Company,{through:Project_company,foreignKey:'project_id'})

Employee.belongsTo(Company,{foreignKey:'company_id'})
Employee.belongsTo(Role,{foreignKey:'role_id'})
Employee.belongsTo(Project,{foreignKey:'project_id'})

//Company.belongsTo(Employee,{foreignKey:'employee_id'})
//Employee.belongsToMany(Company,{through:Employee,foreignKey:'_id'})
//Company.belongsToMany(Employee,{through:Employee,foreignKey:'company_id'})

//Project.belongsTo(Status,{foreignKey:'status_id'})

//Define ReturnType
const returnType = new graphql.GraphQLObjectType({
    name: "returntype",
    fields: {
        id: { type: graphql.GraphQLID } ,
        status:{type: graphql.GraphQLString} 
    }
  });

//Define ReturnType
const StatusType = new graphql.GraphQLObjectType({
    name: "Status",
    fields: {
        id: { type: graphql.GraphQLID } ,
        project_status:{type: graphql.GraphQLString} 
    }
  });

//Company
const CompanyType = new graphql.GraphQLObjectType({
  name: "company",
  fields: {
      id: { type: graphql.GraphQLID },
      company_name: { type: graphql.GraphQLString },
  }
});

//Project
const ProjectType = new graphql.GraphQLObjectType({
    name: "Project",
  fields: {
      id: { type: graphql.GraphQLID },
      project_name: { type: graphql.GraphQLString },
      //status: { type: StatusType}
  } 
})

//Role
const RoleType = new graphql.GraphQLObjectType({
    name: "Role",
  fields: {
      id: { type: graphql.GraphQLID },
      role_type: { type: graphql.GraphQLString },
  } 
})

//Employee
const EmployeeType = new graphql.GraphQLObjectType({
    name: "Employee",
  fields: {
      id: { type: graphql.GraphQLID },
      employee_name: { type: graphql.GraphQLString },
      company: { type:CompanyType},
      role: { type: RoleType},
      project: { type: ProjectType},
  } 
})

//Project_company
const Project_companyType = new graphql.GraphQLObjectType({
    name:'Project_company',
    fields:{
        id: { type : graphql.GraphQLID },
        project_name: { type: graphql.GraphQLString},
        companies: { type: new graphql.GraphQLList(CompanyType)},
    }
})



//Classes Query
var queryType = new graphql.GraphQLObjectType({
    name: 'Query',
    fields: {
    companies: {
        type: graphql.GraphQLList(CompanyType),
        resolve: (root, args, context, info) => {
        return Company.findAll() 
          }
        },
    employees: {
        type: graphql.GraphQLList(EmployeeType),
        resolve: async(root, args, context, info) => {
          let emp = await Employee.findAll({include:[{model:Company},{model:Role},{model:Project},{model:Status}]})
          console.log(emp[0].company)
        return emp 
            }
          },
    employee: {
        type: EmployeeType,
        args: {
        id: {
        type: new graphql.GraphQLNonNull(graphql.GraphQLID)
            }
            },
        resolve: (root, {id}, context, info) => {
            return Employee.findOne({include:[{model:Company},{model:Role},{model:Project}]},{where:{id:id}});  
                }
              },
    projects: {
        type: graphql.GraphQLList(ProjectType),
        resolve: (root, args, context, info) => {
        return Project.findAll() 
            }
          }, 
    project: {
            type: ProjectType,
            args: {
            id: {
            type: new graphql.GraphQLNonNull(graphql.GraphQLID)
                }
                },
            resolve: (root, {id}, context, info) => {
                return Project.findOne({where:{id:id}});  
                    }
                  },
    roles: {
        type: graphql.GraphQLList(RoleType),
        resolve: (root, args, context, info) => {
        return Role.findAll() 
            }
          },  
    project_companies:{
        type : graphql.GraphQLList(Project_companyType),
        resolve:  (root, args, context, info) => {
        return Project.findAll({include:[{model:Company}]});
            }
        },
        project_company: {
          type: Project_companyType,
          args: {
          id: {
          type: new graphql.GraphQLNonNull(graphql.GraphQLID)
              }
              },
          resolve: (root, {id}, context, info) => {
              return Project.findOne({include:[{model:Company}]},{where:{id:id}});  
                  }
                }, 
        status:{
          type : graphql.GraphQLList(StatusType),
        resolve:  (root, args, context, info) => {
        return Status.findAll();
            }
        }
    }
})

//Mutation
var mutationType = new graphql.GraphQLObjectType({
    name : 'Mutation',
    fields:{
    createCompany: {
        type: CompanyType,
        args: { 
          id: {
          type: graphql.GraphQLID
          },
          company_name: {
            type: graphql.GraphQLString
          }
        },
        resolve: (root, {id,company_name,}, context, info) => {
     return Company.create({id:id,company_name:company_name});
        }
      },
    updateCompany: {
      type: returnType,
      args: { 
        id:{
          type: graphql.GraphQLID
        },
        company_name: {
          type: graphql.GraphQLString
        }
      },
      resolve: (root,{id,company_name}, context, info) => {
      
      let update =  Company.update({company_name:company_name},{where:{id:id}})
        return {status:true,id:id}
      }
    },
    destroyCompany : {
      type: returnType,
      args: {
        id : {
          type: graphql.GraphQLID
        }
      },
      resolve: (root,{id}) => {
        let destroy = Company.destroy({where:{id:id}})
        return {status:true,id:id}
      }
    },
    createEmployee: {
        type: EmployeeType,
        args: { 
          id: {
          type: graphql.GraphQLID
          },
          employee_name: {
            type: graphql.GraphQLString
          }
        },
        resolve: (root, {id,employee_name,}, context, info) => {
     return Employee.create({id:id,employee_name:employee_name});
        }
      },
    updateEmployee: {
      type: returnType,
      args: { 
        id:{
          type: graphql.GraphQLID
        },
        employee_name: {
          type: graphql.GraphQLString
        }
      },
      resolve: (root,{id,employee_name}, context, info) => {
      
      let update =  Employee.update({employee_name:employee_name},{where:{id:id}})
        return {status:true,id:id}
      }
    },
    destroyEmployee : {
      type: returnType,
      args: {
        id : {
          type: graphql.GraphQLID
        }
      },
      resolve: (root,{id}) => {
        let destroy = Employee.destroy({where:{id:id}})
        return {status:true,id:id}
      }
    },
    createProject: {
        type: ProjectType,
        args: { 
          id: {
          type: graphql.GraphQLID
          },
          project_name: {
            type: graphql.GraphQLString
          }
        },
        resolve: (root, {id,project_name,}, context, info) => {
     return Project.create({id:id,project_name:project_name});
        }
      },
    updateProject: {
      type: returnType,
      args: { 
        id:{
          type: graphql.GraphQLID
        },
        project_name: {
          type: graphql.GraphQLString
        }
      },
      resolve: (root,{id,project_name}, context, info) => {
      
      let update =  Project.update({project_name:project_name},{where:{id:id}})
        return {status:true,id:id}
      }
    },
    destroyProject : {
      type: returnType,
      args: {
        id : {
          type: graphql.GraphQLID
        }
      },
      resolve: (root,{id}) => {
        let destroy = Project.destroy({where:{id:id}})
        return {status:true,id:id}
      }
    },
    createRole: {
        type: RoleType,
        args: { 
          id: {
          type: graphql.GraphQLID
          },
          role_type: {
            type: graphql.GraphQLString
          }
        },
        resolve: (root, {id,role_type}, context, info) => {
     return Role.create({id:id,role_type:role_type});
        }
      },
    updateRole: {
      type: returnType,
      args: { 
        id:{
          type: graphql.GraphQLID
        },
        role_type: {
          type: graphql.GraphQLString
        }
      },
      resolve: (root,{id,role_type}, context, info) => {
      
      let update =  Role.update({role_type:role_type},{where:{id:id}})
        return {status:true,id:id}
      }
    },
    destroyRole : {
      type: returnType,
      args: {
        id : {
          type: graphql.GraphQLID
        }
      },
      resolve: (root,{id}) => {
        let destroy = Role.destroy({where:{id:id}})
        return {status:true,id:id}
      }
    },
  //   createProject_company: {
  //     type: Project_companyType,
  //     args: { 
  //       id: {
  //       type: graphql.GraphQLID
  //       },
  //       project_id: {
  //         type: graphql.GraphQLID
  //       },
  //       company_id:{
  //         type: graphql.GraphQLID
  //       }
  //     },
  //     resolve: (root, {id,project_id,company_id}, context, info) => {
  //  return Project_company.create({id:id,project_id:project_id,company_id:company_id});
  //     }
  //   },
  // updateProject_company: {
  //   type: returnType,
  //   args: { 
  //     id:{
  //       type: graphql.GraphQLID
  //     },
  //     project_id: {
  //       type: graphql.GraphQLID
  //     },
  //     company_id: {
  //       type: graphql.GraphQLID
  //     }
  //   },
  //   resolve: (root,{id,project_id,company_id}, context, info) => {
    
  //   let update =  Project_company.update({project_id:project_id,company_id:company_id},{where:{id:id}})
  //     return {status:true,id:id}
  //   }
  // },
  // destroyProject_company : {
  //   type: returnType,
  //   args: {
  //     id : {
  //       type: graphql.GraphQLID
  //     }
  //   },
  //   resolve: (root,{id}) => {
  //     let destroy = Project_company.destroy({where:{id:id}})
  //     return {status:true,id:id}
  //   }
  // },
  }
  });

const schema = new graphql.GraphQLSchema({
    query: queryType,
    mutation: mutationType
      });

app.use('/graphql', expressGraphQl({schema: schema,
    graphiql: true}));

app.listen(3000, function(){
  console.log('Server Started')
  })